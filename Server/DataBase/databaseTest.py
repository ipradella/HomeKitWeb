
import unittest
import sqlite3
from sqlite3 import Error
import database

class DatabaseTest(unittest.TestCase):
    def setUp(self):
        self.connection = self.create_connection()

    def tearDown(self):
        self.connection.close()

    def create_connection(self, db_file = ':memory:'):
        connection = None
        try:
            connection = sqlite3.connect(db_file)    
        except Error as e:
            print(e)
    
        return connection

    def test_createtable(self):
        testdb = database.Database(self.connection)
        testdb.create_table_monitoring_temparature(True)
        
        cursor = self.connection.cursor()
        cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='TEMPERATURE' ''') 
        self.assertEqual(cursor.fetchone()[0],1)

if __name__ == '__main__':
    unittest.main()