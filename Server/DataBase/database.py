import sqlite3

class Database:

    def __init__(self, connection):
        self.connection = connection

    def __del__(self):
        try:
            self.connection.close()
        except Exception as e:
            print("Erreur : %s" % e)

    def create_table_monitoring_temparature (self, reset):
        cursor = self.connection.cursor()
        if reset :
            cursor.execute(''' SELECT count(name) FROM sqlite_master WHERE type='table' AND name='TEMPERATURE' ''')
            if cursor.fetchone()[0]==1 : {
                cursor.execute('''DROP TABLE TEMPERATURE''')
            }
        
        cursor.execute('''CREATE TABLE TEMPERATURE ([Date] date PRIMARY KEY,[inside] float, [outside] float)''')
        self.connection.commit()


