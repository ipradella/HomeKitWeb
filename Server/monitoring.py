import datetime
import logging
import time
import random

def main():
    logging.basicConfig(filename='monitoring.log', level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
    logging.info('Started')
    while True:
      logging.info("outside temperature: %s" %random.randrange(0,25))
      time.sleep(5)

    logging.info('Finished')       

if __name__ == '__main__':
    main()